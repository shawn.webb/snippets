/*-
 * Copyright (c) 2021 Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SNIPPETS_H
#define	_SNIPPETS_H

#include <stdbool.h>
#include <sys/queue.h>

#include <curl/curl.h>

#define EXPORTED_SYM __attribute__((visibility("default")))

#define LIBSNIPPETS_VERSION		1
#define	SNIPPET_DEFAULT_FILEPATH	"file.txt"

typedef enum _snippet_visibility {
	PRIVATE = 0,
	PUBLIC = 1,
	INTERNAL = 2
} snippet_visibility_t;

typedef struct _snippet_file {
	int				 sf_fd;
	/* sf_filename: Local file on the filesystem */
	char				*sf_filename;
	/* sf_filepath: GitLab's filepath */
	char				*sf_filepath;
	char				*sf_content;
	SLIST_ENTRY(_snippet_file)	 sf_entry;
} snippet_file_t;

typedef struct _snippet_author {
	uint64_t	 sa_id;
	char		*sa_username;
	char		*sa_email;
	char		*sa_name;
	char		*sa_createdate;
} snippet_author_t;

typedef struct _snippet_ctx {
	uint64_t	 sc_version;
	char		*sc_confpath;
	char		*sc_apitoken;
	char		*sc_site;
	CURL		*sc_curlctx;
} snippet_ctx_t;

typedef struct _snippet {
	uint64_t			 s_id;
	char				*s_url;
	char				*s_title;
	char				*s_description;
	char				*s_project;
	snippet_visibility_t		 s_visibility;
	SLIST_HEAD(, _snippet_file)	 s_files;
	uint64_t			 s_nfiles;
	snippet_ctx_t			*s_ctx;
} snippet_t;

snippet_ctx_t *snippet_ctx_new(void);
snippet_t *snippet_new(snippet_ctx_t *);
bool snippet_set_title(snippet_t *, const char *);
bool snippet_set_description(snippet_t *, const char *);
bool snippet_set_project(snippet_t *, const char *);
bool snippet_set_visibility(snippet_t *, snippet_visibility_t);
bool snippet_add_file(snippet_t *, snippet_file_t *);

snippet_file_t *snippet_file_new(snippet_t *);

#endif /* ! _SNIPPETS_H */
