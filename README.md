# snippets

This project provides easy command-line interface to GitLab's Snippets
feature.

License: 2-Clause BSDL

## Dependencies

* libcurl
* libucl

## Status

This project is in its infancy. There's really nothing to see here,
yet. The ABI and API both are not stable.
