/*-
 * Copyright (c) 2021 Shawn Webb <shawn.webb@hardenedbsd.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "snippets.h"

EXPORTED_SYM
snippet_ctx_t *
snippet_ctx_new(void)
{
	snippet_ctx_t *ctx;

	ctx = calloc(1, sizeof(*ctx));
	if (ctx != NULL) {
		ctx->sc_version = LIBSNIPPETS_VERSION;
	}

	return (ctx);
}

EXPORTED_SYM
snippet_t *
snippet_new(snippet_ctx_t *ctx)
{
	snippet_t *snippet;

	snippet = calloc(1, sizeof(*snippet));
	if (snippet != NULL) {
		snippet->s_ctx = ctx;
		SLIST_INIT(&(snippet->s_files));
	}

	return (snippet);
}

EXPORTED_SYM
bool
snippet_set_title(snippet_t *snip, const char *title)
{

	free(snip->s_title);
	snip->s_title = strdup(title);
	return (snip->s_title != NULL);
}

EXPORTED_SYM
bool
snippet_set_description(snippet_t *snip, const char *description)
{

	free(snip->s_description);
	snip->s_description = strdup(description);
	return (snip->s_description != NULL);
}

EXPORTED_SYM
bool
snippet_set_project(snippet_t *snip, const char *project)
{

	free(snip->s_project);

	/*
	 * Snippets may not relate to a project. Allow API callers to
	 * set the project to NULL.
	 */
	if (project == NULL) {
		snip->s_project = NULL;
		return (true);
	}

	snip->s_project = strdup(project);
	return (snip->s_project != NULL);
}

EXPORTED_SYM
bool
snippet_set_visibility(snippet_t *snip, snippet_visibility_t vis)
{

	snip->s_visibility = vis;
	return (true);
}

EXPORTED_SYM
bool
snippet_add_file(snippet_t *snip, snippet_file_t *file)
{

	SLIST_INSERT_HEAD(&(snip->s_files), file, sf_entry);
	snip->s_nfiles++;
	return (true);
}

EXPORTED_SYM
snippet_file_t *
snippet_file_new(snippet_t *snip)
{
	snippet_file_t *file;

	file = calloc(1, sizeof(*file));
	if (file != NULL) {
		if (!snippet_add_file(snip, file)) {
			free(file);
			return (false);
		}
	}

	return (file);
}
